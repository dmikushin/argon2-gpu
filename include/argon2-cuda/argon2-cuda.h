#ifndef ARGON2_CUDA_H
#define ARGON2_CUDA_H

#ifdef WIN32
#ifdef argon2_cuda_EXPORTS
#define ARGON2_CUDA_API __declspec(dllexport)
#else
#define ARGON2_CUDA_API __declspec(dllimport)
#endif
#else
#define ARGON2_CUDA_API
#endif // WIN32

#endif // ARGON2_CUDA_H
