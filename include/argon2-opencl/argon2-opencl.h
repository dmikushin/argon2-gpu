#ifndef ARGON2_OPENCL_H
#define ARGON2_OPENCL_H

#ifdef WIN32
#ifdef argon2_opencl_EXPORTS
#define ARGON2_OPENCL_API __declspec(dllexport)
#else
#define ARGON2_OPENCL_API __declspec(dllimport)
#endif
#else
#define ARGON2_OPENCL_API
#endif // WIN32

#endif // ARGON2_OPENCL_H
